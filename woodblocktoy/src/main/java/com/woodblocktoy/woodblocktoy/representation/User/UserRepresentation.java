package com.woodblocktoy.woodblocktoy.representation.User;


public class UserRepresentation {

    String id;
    String nom;
    String prenom;
    String addrPostale;
    String email;
    String numTelephone;


    public UserRepresentation(String id, String nom, String prenom, String addrPostale, String email, String numTelephone) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.addrPostale = addrPostale;
        this.email = email;
        this.numTelephone = numTelephone;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAddrPostale() {
        return addrPostale;
    }

    public void setAddrPostale(String addrPostale) {
        this.addrPostale = addrPostale;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumTelephone() {
        return numTelephone;
    }

    public void setNumTelephone(String numTelephone) {
        this.numTelephone = numTelephone;
    }


}
