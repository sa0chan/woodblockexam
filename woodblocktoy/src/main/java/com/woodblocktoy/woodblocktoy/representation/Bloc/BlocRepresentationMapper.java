package com.woodblocktoy.woodblocktoy.representation.Bloc;

import com.woodblocktoy.woodblocktoy.domains.Bloc.Bloc;
import com.woodblocktoy.woodblocktoy.repositories.BlocRepository;
import org.springframework.stereotype.Component;

@Component
public class BlocRepresentationMapper {

    private BlocRepository repository;

    public BlocRepresentationMapper(BlocRepository repository) {
        this.repository = repository;
    }

    public BlocRepresentation mapBlocToBlocRepresentation(Bloc bloc) {

        BlocRepresentation result =
                new BlocRepresentation(bloc.getId(),bloc.getVolume(), bloc.getHauteur(), bloc.getBaseGeometrique(), bloc.getCouleur(),bloc.getFinitions(), bloc.getEssences());
        result.setId(bloc.getId());
        return result;
    }

    public Bloc mapBlocRepresentationToBloc(BlocRepresentation blocRepresentation) {
        Bloc result =
                new Bloc(blocRepresentation.getId(),blocRepresentation.getVolume(), blocRepresentation.getHauteur(), blocRepresentation.getBaseGeometrique(),
                        blocRepresentation.getCouleur(), blocRepresentation.getFinitions(),
                        blocRepresentation.getEssences());
        return result;
    }
}
