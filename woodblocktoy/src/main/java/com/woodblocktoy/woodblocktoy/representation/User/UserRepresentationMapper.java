package com.woodblocktoy.woodblocktoy.representation.User;

import com.woodblocktoy.woodblocktoy.domains.User;
import com.woodblocktoy.woodblocktoy.repositories.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserRepresentationMapper {
    private UserRepository repository;

    public UserRepresentationMapper(UserRepository repository) {
        this.repository = repository;
    }

    public UserRepresentation mapUserToUserRepresentation(User user) {

        UserRepresentation result =
                new UserRepresentation(user.getId(), user.getNom(), user.getPrenom(), user.getAddrPostale(),
                        user.getEmail(), user.getNumTelephone());
        result.setId(user.getId());
        return result;
    }

    public User mapUserRepresentationToUser(UserRepresentation userRepresentation) {
        User result =
                new User(userRepresentation.getId(), userRepresentation.getNom(), userRepresentation.getPrenom(),
                        userRepresentation.getAddrPostale(), userRepresentation.getEmail(),
                        userRepresentation.getNumTelephone());
        return result;
    }
}
