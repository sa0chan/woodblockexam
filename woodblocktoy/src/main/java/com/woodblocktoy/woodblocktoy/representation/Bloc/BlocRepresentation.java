package com.woodblocktoy.woodblocktoy.representation.Bloc;

import com.woodblocktoy.woodblocktoy.domains.Bloc.*;


public class BlocRepresentation {

    String id;
    Double volume;
    Hauteur hauteur;
    BaseGeometrique baseGeometrique;
    Couleur couleur;
    Finitions finitions;
    Essences essences;


    public BlocRepresentation(String id , Double volume, Hauteur hauteur, BaseGeometrique baseGeometrique, Couleur couleur, Finitions finitions, Essences essences ){
        this.id = id ;
        this.volume = volume ;
        this.hauteur = hauteur;
        this.baseGeometrique = baseGeometrique;
        this.couleur = couleur;
        this.finitions = finitions;
        this.essences = essences;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Hauteur getHauteur() {
        return hauteur;
    }

    public void setHauteur(Hauteur hauteur) {
        this.hauteur = hauteur;
    }

    public BaseGeometrique getBaseGeometrique() {
        return baseGeometrique;
    }

    public void setBaseGeometrique(BaseGeometrique baseGeometrique) {
        this.baseGeometrique = baseGeometrique;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public Finitions getFinitions() {
        return finitions;
    }

    public void setFinitions(Finitions finitions) {
        this.finitions = finitions;
    }

    public Essences getEssences() {
        return essences;
    }

    public void setEssences(Essences essences) {
        this.essences = essences;
    }

}
