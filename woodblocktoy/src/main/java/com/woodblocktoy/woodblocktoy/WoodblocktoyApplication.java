package com.woodblocktoy.woodblocktoy;

import com.woodblocktoy.woodblocktoy.repositories.UserRepository;
import com.woodblocktoy.woodblocktoy.services.IdGenerator;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WoodblocktoyApplication {

	public static void main(String[] args) {
		SpringApplication.run(WoodblocktoyApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(UserRepository repository, IdGenerator idGenerator) {
		return (args) -> {

		};
	}

}
