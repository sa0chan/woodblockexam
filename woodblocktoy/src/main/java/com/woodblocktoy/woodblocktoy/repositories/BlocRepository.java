package com.woodblocktoy.woodblocktoy.repositories;

import com.woodblocktoy.woodblocktoy.domains.Bloc.Bloc;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BlocRepository extends CrudRepository<Bloc, String> {
    List<Bloc> findByid(String id);

    Optional<Bloc> findById(String id);
}
