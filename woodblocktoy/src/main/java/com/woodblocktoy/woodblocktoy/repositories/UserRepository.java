package com.woodblocktoy.woodblocktoy.repositories;

import com.woodblocktoy.woodblocktoy.domains.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
    List<User> findByNom(String nom);

    Optional<User> findById(String id);

}
