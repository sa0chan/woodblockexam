package com.woodblocktoy.woodblocktoy.domains.Bloc;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="blocs")
public class Bloc {

    @Id
    String id;
    Double volume;
    Hauteur hauteur;
    BaseGeometrique baseGeometrique;
    Couleur couleur;
    Finitions finitions;
    Essences essences;

    protected Bloc(){
    }

    public Bloc(String id , Double volume, Hauteur hauteur, BaseGeometrique baseGeometrique, Couleur couleur, Finitions finitions, Essences essences ){
        this.id = id ;
        this.hauteur = hauteur;
        this.baseGeometrique = baseGeometrique;
        this.couleur = couleur;
        this.finitions = finitions;
        this.essences = essences;
        this.volume = volume;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Hauteur getHauteur() {
        return hauteur;
    }

    public void setHauteur(Hauteur hauteur) {
        this.hauteur = hauteur;
    }

    public BaseGeometrique getBaseGeometrique() {
        return baseGeometrique;
    }

    public void setBaseGeometrique(BaseGeometrique baseGeometrique) {
        this.baseGeometrique = baseGeometrique;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public Finitions getFinitions() {
        return finitions;
    }

    public void setFinitions(Finitions finitions) {
        this.finitions = finitions;
    }

    public Essences getEssences() {
        return essences;
    }

    public void setEssences(Essences essences) {
        this.essences = essences;
    }

}
