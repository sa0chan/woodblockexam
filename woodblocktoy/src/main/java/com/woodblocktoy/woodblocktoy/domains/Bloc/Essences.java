package com.woodblocktoy.woodblocktoy.domains.Bloc;

public enum Essences {
    chene ("chene",900),
    pin ("pin",200),
    hetre ("hetre",300),
    acajou ("acajou",40000);
    private Integer prix ;
    private String essence;

    //Constructeur
    Essences(String essence, Integer prix){
        this.prix = prix;
        this.essence = essence;
    }
}
