package com.woodblocktoy.woodblocktoy.domains.Bloc;

public enum Finitions {
    mate ("mate", 0.30),
    brillante ("brillante",0.90),
    satinee ("satinee",0.50),
    brute ("brute",0.0);
    private Double prix ;
    private String typeFinition;

    //Constructeur
    Finitions(String typeFinition, Double prix){
        this.prix = prix;
        this.typeFinition = typeFinition;
    }
}
