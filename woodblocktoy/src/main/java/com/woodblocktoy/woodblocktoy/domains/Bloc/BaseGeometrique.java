package com.woodblocktoy.woodblocktoy.domains.Bloc;

public enum BaseGeometrique {
    carree,
    triangulaire,
    hexagonale,
    rectangulaire,
    cylindre;

}
