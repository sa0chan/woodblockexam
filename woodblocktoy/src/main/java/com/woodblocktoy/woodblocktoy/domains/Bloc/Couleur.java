package com.woodblocktoy.woodblocktoy.domains.Bloc;

public enum Couleur {
    aucune,
    bleu,
    blanc,
    rouge,
    jaune,
    orange,
    vert,
    noir;
}
