package com.woodblocktoy.woodblocktoy.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {
    @Id
    String id;
    String nom;
    String prenom;
    String addrPostale;
    String email;
    String numTelephone;

    //constructeur par defaut
    protected User(){

    }
    public User(String id, String nom, String prenom, String addrPostale, String email, String numTelephone) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.addrPostale = addrPostale;
        this.email = email;
        this.numTelephone = numTelephone;
    }

    @Override
    public String toString() {
        return String.format(
                "User[id=%s, nom='%s', prenom='%s',origine='%s',addrPostale='%s',email='%s',numTelephone='%s']",
                id, nom, prenom, addrPostale, email, numTelephone);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAddrPostale() {
        return addrPostale;
    }

    public void setAddrPostale(String addrPostale) {
        this.addrPostale = addrPostale;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getNumTelephone() {
        return numTelephone;
    }

    public void setNumTelephone(String numTelephone) {
        this.numTelephone = numTelephone;
    }


}
