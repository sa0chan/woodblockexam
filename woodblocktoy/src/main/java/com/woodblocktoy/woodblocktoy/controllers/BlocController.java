package com.woodblocktoy.woodblocktoy.controllers;

import com.woodblocktoy.woodblocktoy.domains.Bloc.Bloc;
import com.woodblocktoy.woodblocktoy.representation.Bloc.BlocRepresentation;
import com.woodblocktoy.woodblocktoy.representation.Bloc.BlocRepresentationMapper;
import com.woodblocktoy.woodblocktoy.services.BlocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/blocs")
public class BlocController {

    private BlocService service;
    private BlocRepresentationMapper mapper;


    @Autowired
    public BlocController(BlocService service, BlocRepresentationMapper mapper) {
        this.service = service;
        this.mapper = mapper;

    }

    @GetMapping
    List<BlocRepresentation> getAllBloc() {

        return this.service.getAllBloc().stream()
                .map(this.mapper::mapBlocToBlocRepresentation)
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    ResponseEntity<BlocRepresentation> getOne(@PathVariable("id") String id) {
        Optional<Bloc> bloc = this.service.getOneBloc(id);

        return bloc
                .map(this.mapper::mapBlocToBlocRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    @PostMapping
    BlocRepresentation createBloc(@RequestBody BlocRepresentation body) {
        final Bloc blocCree;
        blocCree = this.mapper.mapBlocRepresentationToBloc(body);
        this.service.createBloc(blocCree);
        return this.mapper.mapBlocToBlocRepresentation(blocCree);

    }

    @DeleteMapping("/{id}")
    void deletebyId(@PathVariable("id") String id) {
        this.service.deleteBloc(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BlocRepresentation> updateBloc(@PathVariable("id") String id,
                                                         @RequestBody BlocRepresentation body) {
        this.service.updateOneBloc(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
