package com.woodblocktoy.woodblocktoy.controllers;

import com.woodblocktoy.woodblocktoy.domains.User;
import com.woodblocktoy.woodblocktoy.representation.User.UserRepresentation;
import com.woodblocktoy.woodblocktoy.representation.User.UserRepresentationMapper;
import com.woodblocktoy.woodblocktoy.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService service;
    private UserRepresentationMapper mapper;


    @Autowired
    public UserController(UserService service, UserRepresentationMapper mapper) {
        this.service = service;
        this.mapper = mapper;

    }

    @GetMapping
    List<UserRepresentation> getAll() {

        return this.service.getAllUsers().stream()
                .map(this.mapper::mapUserToUserRepresentation)
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    ResponseEntity<UserRepresentation> getOne(@PathVariable("id") String id) {
        Optional<User> user = this.service.getOneUser(id);

        return user
                .map(this.mapper::mapUserToUserRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    @PostMapping
    UserRepresentation createUser(@RequestBody UserRepresentation body) {
        final User userCree;
        userCree = this.mapper.mapUserRepresentationToUser(body);
        this.service.createUser(userCree);
        return this.mapper.mapUserToUserRepresentation(userCree);

    }

    @DeleteMapping("/{id}")
    void deletebyId(@PathVariable("id") String id) {
        this.service.deleteUser(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserRepresentation> updateUser(@PathVariable ("id") String id ,
                                                             @RequestBody UserRepresentation body) {
        this.service.updateOneUser(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }



}
