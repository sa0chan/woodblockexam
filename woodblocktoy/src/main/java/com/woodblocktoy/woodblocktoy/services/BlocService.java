package com.woodblocktoy.woodblocktoy.services;

import com.woodblocktoy.woodblocktoy.domains.Bloc.*;
import com.woodblocktoy.woodblocktoy.repositories.BlocRepository;
import com.woodblocktoy.woodblocktoy.representation.Bloc.BlocRepresentation;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class BlocService {

    IdGenerator idGenerator;

    private BlocRepository repository;

    public BlocService(BlocRepository repository, IdGenerator idGenerator) {
        this.repository = repository;
        this.idGenerator = idGenerator;
    }

    public List<Bloc> getAllBloc() {
        List<Bloc> blocList = new ArrayList<>();
        repository.findAll().forEach(blocList::add);
        return blocList;
    }

    public Optional<Bloc> getOneBloc(String id) {
        return repository.findById(id);
    }

    //a mettre quand on sait que la methode peut échouer : par exemple pas d'autorisation de plus de 10 perso par player
    @Transactional(rollbackOn = Exception.class)
    public Bloc createBloc(Bloc bloc) {
        bloc.setId(idGenerator.generateNewId());

      /*  if (!bloc.getCouleur().equals(Couleur.aucune)) {
            bloc.setEssences(Essences.pin);
            repository.save(bloc);
            return bloc;
        }else {
            bloc.setCouleur(Couleur.aucune);
            repository.save(bloc);
            return bloc;
        }*/
        if (bloc.getEssences().equals(Essences.pin)) {
            repository.save(bloc);
            return bloc;
        }else {
            bloc.setCouleur(Couleur.aucune);
            repository.save(bloc);
            return bloc;
        }

    }

    public void deleteBloc(String id) {
        repository.deleteById(id);
    }

    public Bloc updateOneBloc(String id, BlocRepresentation body) {
        Bloc blocToModify = getOneBloc(id).orElseThrow();
        Bloc blocInBase = new Bloc(this.idGenerator.generateNewId(), body.getVolume(), body.getHauteur(), body.getBaseGeometrique(),
                body.getCouleur(), body.getFinitions(), body.getEssences());

        Hauteur hauteur;
        BaseGeometrique baseGeometrique;
        Couleur couleur;
        Finitions finitions;
        Double volume;
        Essences essences;

        if (blocInBase.getHauteur().equals("")) {
            hauteur = blocToModify.getHauteur();
        } else {
            hauteur = blocInBase.getHauteur();
        }
        if (blocInBase.getBaseGeometrique().equals("")) {
            baseGeometrique = blocToModify.getBaseGeometrique();
        } else {
            baseGeometrique = blocInBase.getBaseGeometrique();
        }

        if (blocInBase.getCouleur().equals("")) {
            couleur = blocToModify.getCouleur();
        } else {
            couleur = blocInBase.getCouleur();
        }
        if (blocInBase.getEssences().equals("")) {
            essences = blocToModify.getEssences();
        } else {
            essences = blocInBase.getEssences();
        }
        if (blocInBase.getFinitions().equals("")) {
            finitions = blocToModify.getFinitions();
        } else {
            finitions = blocInBase.getFinitions();
        }

        if (blocInBase.getVolume().equals("")) {
            volume = blocInBase.getVolume();
        } else {
            volume = blocInBase.getVolume();
        }
        Bloc bloc = new Bloc(id, volume, hauteur, baseGeometrique, couleur, finitions, essences);
        return this.repository.save(bloc);
    }


}