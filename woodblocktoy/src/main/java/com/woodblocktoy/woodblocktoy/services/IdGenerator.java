package com.woodblocktoy.woodblocktoy.services;

import org.springframework.stereotype.Component;
import java.util.UUID;

@Component
public class IdGenerator {
    //private final AtomicLong counter = new AtomicLong();

    public String generateNewId() {
        return UUID.randomUUID().toString();
    }
}

