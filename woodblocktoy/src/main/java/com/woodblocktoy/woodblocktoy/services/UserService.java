package com.woodblocktoy.woodblocktoy.services;

import com.woodblocktoy.woodblocktoy.domains.User;
import com.woodblocktoy.woodblocktoy.repositories.UserRepository;
import com.woodblocktoy.woodblocktoy.representation.User.UserRepresentation;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component

public class UserService {

    IdGenerator idGenerator;

    private UserRepository repository;

    public UserService(UserRepository repository, IdGenerator idGenerator) {
        this.repository = repository;
        this.idGenerator = idGenerator;
    }

    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<>();
        repository.findAll().forEach(userList::add);
        return userList;
    }

    public Optional<User> getOneUser(String id) {
        return repository.findById(id);
    }

    //a mettre quand on sait que la methode peut échouer : par exemple pas d'autorisation de plus de 10 perso par player
    @Transactional(rollbackOn = Exception.class)
    public User createUser(User user) {
        user.setId(idGenerator.generateNewId());
        repository.save(user);
        return user;
    }

    public void deleteUser(String id) {
        repository.deleteById(id);
    }

    public User updateOneUser(String id, UserRepresentation body) {
        User userToModify = getOneUser(id).orElseThrow();
        User userInBase = new User(this.idGenerator.generateNewId(), body.getNom(), body.getPrenom(),
                body.getAddrPostale(), body.getEmail(), body.getNumTelephone());

        String nom = "";
        String prenom = "";
        String addrPostale = "";
        String email = "";
        String numTelephone = "";

        if (userInBase.getNom().equals("")) {
            nom = userToModify.getNom();
        } else {
            nom = userInBase.getNom();
        }
        if (userInBase.getPrenom().equals("")) {
            prenom = userToModify.getPrenom();
        } else {
            prenom = userInBase.getPrenom();
        }
        if (userInBase.getAddrPostale().isEmpty()) {
            addrPostale = userToModify.getAddrPostale();
        } else {
            addrPostale = userInBase.getAddrPostale();
        }
        if (userInBase.getEmail().equals("")) {
            email = userToModify.getEmail();
        } else {
            email = userInBase.getEmail();
        }
        if (userInBase.getNumTelephone().equals("")) {
            numTelephone = userToModify.getNumTelephone();
        } else {
            numTelephone = userInBase.getNumTelephone();
        }
        User newUser = new User(id, nom, prenom, addrPostale,email, numTelephone);
        return this.repository.save(newUser);
    }


}


