--création table user

Create table users(
 	id varchar,
 	nom varchar,
 	prenom varchar,
 	addr_postale varchar,
 	email varchar,
 	num_telephone varchar,
 	primary key (id)
 )

--creation table blocs
Create table blocs(
 	id varchar,
 	hauteur varchar,
 	base_geomatique varchar,
 	couleur varchar,
 	finitions varchar,
 	essences varchar,
 	volume float,
 	primary key (id)
 )